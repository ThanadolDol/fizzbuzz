import { fizzBuzz } from ".";

describe("fizzBuzz", () => {
    it("should return 1", () => {
      expect(fizzBuzz(1)).toEqual("1");
    });
  
    it("should return 2", () => {
      expect(fizzBuzz(2)).toEqual("2");
    });
  
    it("should return Fizz", () => {
      expect(fizzBuzz(3)).toEqual("Fizz");
    });
  
    it("should return 4", () => {
      expect(fizzBuzz(4)).toEqual("4");
    });
  
    it("should return Buzz", () => {
      expect(fizzBuzz(5)).toEqual("Buzz");
    });
  
    it("should return Fizz", () => {
      expect(fizzBuzz(6)).toEqual("Fizz");
    });
  
    it("should return 7", () => {
      expect(fizzBuzz(7)).toEqual("7");
    });
  
    it("should return 8", () => {
      expect(fizzBuzz(8)).toEqual("8");
    });
  
    it("should return Fizz", () => {
      expect(fizzBuzz(9)).toEqual("Fizz");
    });
  
    it("should return Buzz", () => {
      expect(fizzBuzz(10)).toEqual("Buzz");
    });
  
    it("should return 11", () => {
      expect(fizzBuzz(11)).toEqual("11");
    });
  
    it("should return Fizz", () => {
      expect(fizzBuzz(12)).toEqual("Fizz");
    });
  
    it("should return 13", () => {
      expect(fizzBuzz(13)).toEqual("13");
    });
  
    it("should return 14", () => {
      expect(fizzBuzz(14)).toEqual("14");
    });
  
    it("should return FizzBuzz", () => {
      expect(fizzBuzz(15)).toEqual("FizzBuzz");
    });
  });
  