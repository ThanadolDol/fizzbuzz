export function fizzBuzz(num: number): string | undefined {
    const isDivisibleByThree: boolean = num % 3 === 0;
    const isDivisibleByFive: boolean = num % 5 === 0;
  
    const fizzMap = new Map<boolean, string>([
        [true, "Fizz"],
        [false, ""]
    ]);
    
    const buzzMap = new Map<boolean, string>([
        [true, "Buzz"],
        [false, ""]
    ]);

    let fizzBuzzStr = "";
    fizzBuzzStr += fizzMap.get(isDivisibleByThree);
    fizzBuzzStr += buzzMap.get(isDivisibleByFive);
  
    const isFizzBuzz = isDivisibleByThree || isDivisibleByFive;

    const resultMap = new Map<boolean,string>([
        [true, fizzBuzzStr],
        [false, num.toString()]
    ]);
  
    return resultMap.get(isFizzBuzz);
}