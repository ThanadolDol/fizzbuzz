package main

import (
	"strconv"
)

func fizzBuzz(num int) string {
	threeFlag := num%3 == 0
	fiveFlag := num%5 == 0
	fizzBuzzStr := map[bool]string{true: "Fizz", false: ""}[threeFlag]
	fizzBuzzStr += map[bool]string{true: "Buzz", false: ""}[fiveFlag]
	fizzBuzzFlag := threeFlag || fiveFlag
	result := map[bool]string{true: fizzBuzzStr, false: strconv.Itoa(num)}[fizzBuzzFlag]
	return result
}
