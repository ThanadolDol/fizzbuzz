package main

import (
	"testing"
)

func Test_fizzBuzz(t *testing.T) {
	type args struct {
		num int
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		// TODO: Add test cases.
		{
			name: "",
			args: args{1},
			want: "1",
		},
		{
			name: "",
			args: args{2},
			want: "2",
		},
		{
			name: "",
			args: args{3},
			want: "Fizz",
		},
		{
			name: "",
			args: args{4},
			want: "4",
		},
		{
			name: "",
			args: args{5},
			want: "Buzz",
		},
		{
			name: "",
			args: args{6},
			want: "Fizz",
		},
		{
			name: "",
			args: args{7},
			want: "7",
		},
		{
			name: "",
			args: args{8},
			want: "8",
		},
		{
			name: "",
			args: args{9},
			want: "Fizz",
		},
		{
			name: "",
			args: args{10},
			want: "Buzz",
		},
		{
			name: "",
			args: args{11},
			want: "11",
		},
		{
			name: "",
			args: args{12},
			want: "Fizz",
		},
		{
			name: "",
			args: args{13},
			want: "13",
		},
		{
			name: "",
			args: args{14},
			want: "14",
		},
		{
			name: "",
			args: args{15},
			want: "FizzBuzz",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := fizzBuzz(tt.args.num); got != tt.want {
				t.Errorf("fizzBuzz() = %v, want %v", got, tt.want)
			}
		})
	}
}
