# FizzBuzz Solver

This project provides GO and TypeScript implementation of the classic FizzBuzz programming challenge. 

## Description

FizzBuzz is a simple yet fun programming exercise that helps you practice loops, conditionals, and basic algorithms. Here's the gist of it:

- Print the numbers from 1 to a given limit (e.g., 100).
- For multiples of 3, print "Fizz" instead of the number.
- For multiples of 5, print "Buzz" instead of the number.
- For numbers that are multiples of both 3 and 5, print "FizzBuzz".